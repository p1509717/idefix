import dbus
import time
from dbus.mainloop.glib import DBusGMainLoop
import dbus.mainloop.glib
from gi.repository import GObject as gobject
import sys
from optparse import OptionParser
import enum
from moveTypes import moveTypes
from moveTypes import Color

gobject.threads_init()
dbus.mainloop.glib.threads_init()
 
proxSensorsVal=[0,0,0,0,0]
ground=[0,0]
led=[]

class AsebaException(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class Aseba(object):

    NB_EVTS_FREQ = 10 # nb of events to be received before computing frequency

    def __init__(self, system_bus = False, dummy = False):
        self.dummy = dummy

        self.callbacks = {}
        self._events_last_evt = {}
        self._events_periods = {}
        self.events_freq = {}

        if not dummy:
            DBusGMainLoop(set_as_default=True)
            # init 
            if system_bus:
                self.bus = dbus.SystemBus()
            else:
                self.bus = dbus.SessionBus()

            try:
                self.network = dbus.Interface(
                        self.bus.get_object('ch.epfl.mobots.Aseba', '/'), 
                        dbus_interface='ch.epfl.mobots.AsebaNetwork')
            except dbus.exceptions.DBusException:
                raise AsebaException("Can not connect to Aseba DBus services! "
                                     "Is asebamedulla running?")


            # Configure event management
            eventfilter = self.network.CreateEventFilter()
            self.events = dbus.Interface(
                        self.bus.get_object('ch.epfl.mobots.Aseba', eventfilter), 
                        dbus_interface='ch.epfl.mobots.EventFilter')
            self.dispatch_handler = self.events.connect_to_signal('Event', self._dispatch_events)

    def clear_events(self):
        """ Use DBus introspection to get the list of event filters, and remove them.
        """
        try:
            introspect = dbus.Interface(
                        self.bus.get_object('ch.epfl.mobots.Aseba', "/events_filters"), 
                        dbus_interface=dbus.INTROSPECTABLE_IFACE)
            interface = introspect.Introspect()
        except dbus.exceptions.DBusException:
            # /events_filters not yet created -> no events to delete
            return

        import xml.etree.ElementTree as ET
        root = ET.fromstring(interface)
        for n in root.iter("node"):
            if 'name' in n.attrib:
                evtfilter = dbus.Interface(
                        self.bus.get_object('ch.epfl.mobots.Aseba', "/events_filters/%s" % n.attrib['name']), 
                        dbus_interface='ch.epfl.mobots.EventFilter')
                evtfilter.Free()

    def __enter__(self):
        self.run()
        return self

    def __exit__(self, type, value, traceback):
        self.close()

    def run(self):
        # run event loop
        self.loop = gobject.MainLoop()
        self.loop.run()

    def close(self):
        self.loop.quit()
        if not self.dummy:
            self.dispatch_handler.remove()
            self.events.Free()

    def dbus_reply(self):
        # correct replay on D-Bus, ignore
        pass

    def dbus_error(self, e):
        assert(not self.dummy)

        # there was an error on D-Bus, stop loop
        self.close()
        raise Exception('dbus error: %s' % str(e))

    def get_nodes_list(self):
        if self.dummy: return []

        dbus_array = self.network.GetNodesList()
        size = len(dbus_array)
        return [str(dbus_array[x]) for x in range(0,size)]

    def set(self, node, var, value):
        if self.dummy: return
        self.network.SetVariable(node, var, value)

    def get(self, node, var):
        if self.dummy: return [0] * 10
        dbus_array = self.network.GetVariable(node, var)
        size = len(dbus_array)
        if (size == 1):
            return int(dbus_array[0])
        else:
            return [int(dbus_array[x]) for x in range(0,size)]

    def send_event(self, event_id, event_args):

        if isinstance(event_id, str):
            return self.send_event_name(event_id, event_args)

        if self.dummy: return

        # events are sent asynchronously
        self.network.SendEvent(event_id, 
                               event_args,
                               reply_handler=self.dbus_reply,
                               error_handler=self.dbus_error,
                               signature='as')


    def send_event_name(self, event_name, event_args):
        if self.dummy: return

        # events are sent asynchronously
        self.network.SendEventName(event_name, 
                                   event_args,
                                   reply_handler=self.dbus_reply,
                                   error_handler=self.dbus_error)


    def load_scripts(self, path):
        """ Loads a given Aseba script (aesl) on the Aseba network.
        """
        if self.dummy: return
        self.network.LoadScripts(path)

    def _dispatch_events(self, *args):
        id, name, vals = args

        key = None

        if name in self.callbacks:
            key = name
        elif id in self.callbacks:
            # event registered from ID, not from name
            key = id

        if key:
            self.callbacks[key](vals)

            # update frequency for this event
            now = time.time()
            self._events_periods[key].append(now - self._events_last_evt[key])
            self._events_last_evt[key] = now
            if len(self._events_periods[key]) == Aseba.NB_EVTS_FREQ:
                self.events_freq[key] = 1. / (sum(self._events_periods[key]) / float(Aseba.NB_EVTS_FREQ))
                self._events_periods[key] = []

    def on_event(self, event_id, cb):
        """
        Subscribe to an Aseba event

        :param event_id: the event name or the event numerical ID
        :param cb: the callback function that will be called with the content of the event as parameter
        """
        if self.dummy: return

        self.callbacks[event_id] = cb
        self._events_last_evt[event_id] = time.time()
        self._events_periods[event_id] = []
        self.events_freq[event_id] = 0.

        if isinstance(event_id, str):
            self.events.ListenEventName(event_id)
        else:
            self.events.ListenEvent(event_id)


   

######################################
        #Color
######################################


######################################
        #State
######################################
class State(enum.Enum):
   Normal = 0
   Reading = 1
   Reading2 = 2
   Reading3 = 3
   Reading4 = 4
   Stop = 5
   Evaluate = 6
   Moving = 7

######################################
        #Robot
######################################    
class Robot :

    def __init__(self,speed) :
        self.SIGMA = 50
        self.speed = speed
        #var target_speed
        self.left = 0
        self.right = 0
        # mean and vari(ance) calculation vars
        self.min_delta = 1024
        self.max_delta = 0
        self.mean = 512
        self.vari = 512
        # actual value and n(ormalized)dev(iation)
        self.delta = aseba.get("thymio-II", "prox.ground.delta")[1]
        self.ndev = 0
        # pi controller vars
        self.ireg = 0
        self.preg = 0
        self.delta_l = 0
        self.main_timer = 0
        self.timer = 0
        self.state = State.Normal
        self.colors = []
        self.timerOn = False
        self.code = []
        self.moveS = False
        self.lastCode = []

######################################
        #timer
######################################

    def display_timer (self) :
        print('value : ' + str(self.timer) + ' state : ' + str(self.timerOn))
           
    def init_timer (self,duration) :
        self.timer = duration
        self.timerOn = True

    def dec_timer (self) :
        self.timer -= 1
    
    def times_up(self) :
        return (self.timer == 0 and self.timerOn == True)
        
    def reset_timer(self) :
        self.timerOn = False
        self.timer = 0
######################################
        #colors
######################################

    def reset_colors(self):
        del self.colors[:]

    def add_color(self,color) :
        self.colors.append(color)
    
    def get_best_color(self,color) :
        if ( abs(78-color) < abs(521-color) and abs(78-color) < abs(850-color) ) :
            return Color.Black
        elif ( abs(521-color) < abs(78-color) and abs(521-color) < abs(850-color) ) :
            return Color.DarkGrey
        elif ( abs(850-color) < abs(78-color) and abs(850-color) < abs(521-color) ) :
            return Color.LightGrey
        else :
            return Color.Undefined
        
    
    def find_color(self,color) :
        if ( 45 < color and color < 112 ) : #78
            return Color.Black
        elif ( 488 < color and color < 554  ) : #521
            return Color.DarkGrey
        elif ( 818 < color and color < 883) : #850
            return Color.LightGrey
        elif ( 901 < color ) :
            return Color.White
        else :
            return (self.get_best_color(color))

            
    
    def read_square (self,state):    
        if(self.timerOn == False) :
            self.init_timer(5)
        elif (self.times_up()) :
            self.reset_timer()
            self.update_state(state)
        else :
            if(self.timer > 2 and self.timer < 7 - self.timer) :
                delta = aseba.get("thymio-II", "prox.ground.delta")[0]#/1024.0
                listaff = [self.timer,delta]
                self.add_color(delta)
                #else :            
        self.fill_lign()              
        self.dec_timer()    

######################################
        #code
######################################

    def reset_code(self):
        del self.code[:]

    def add_code(self,square) : 
        self.code.append(square)
    
    def read_code(self,state) :
            self.read_square(state)
    
    def evaluate_code(self) :
        print('Code : '+str(self.code))
        self.moveS = True

        if moveTypes.get(tuple(self.code)) != None :
            self.state = State.Moving
            
        else :
            self.moveS = False
            self.state = State.Normal
        self.lastCode = self.code
        self.code = []

######################################
        #fill_ligne
######################################
                  
    def update_min (self) :
        if self.min_delta > self.delta :
            self.min_delta = self.delta

    def update_max (self) :
        if self.max_delta < self.delta :
            self.max_delta = self.delta
            
    def update_vari (self) :
        self.vari = int ( 0.45 * (self.max_delta - self.min_delta) )
    
    def update_mean (self) :
        self.mean = int ( (self.max_delta + self.min_delta) / 2 )
    
    def update_ndev (self) :
        self.ndev = int( ( self.SIGMA * (self.delta - self.mean) ) / self.vari )
    
    def statistics (self) :
        self.update_max()
        self.update_min()
        if ( (self.max_delta - self.min_delta ) > 400 ) :
            self.update_vari()
            self.update_mean()

######################################
        #global
######################################

    def stop(self,duration):
        
        if(self.timerOn == False) :
            
            self.init_timer(duration)
        elif (self.times_up()) :
            
            self.reset_timer()
            self.state = State.Reading
        else :
            aseba.set("thymio-II","motor.left.target",[0])
            aseba.set("thymio-II","motor.right.target",[0])
        self.dec_timer()
        
    def evaluate(self):
        self.evaluate_code()
        self.fill_lign()

    def update_state (self,state):
        
        avg_value = sum(self.colors) / len(self.colors)
        color = self.find_color(avg_value)
        self.add_code(color)

        if(state == State.Reading ):
            self.reset_colors()
            self.state = State.Reading2
        elif(state == State.Reading2 ):
            self.reset_colors()
            self.state = State.Reading3
        elif(state == State.Reading3 ):
            self.reset_colors()
            self.state = State.Reading4
        elif(state == State.Reading4 ):
            self.reset_colors()
            self.state = State.Evaluate
                      
    def left_sensor(self) :
        self.delta_l = aseba.get("thymio-II", "prox.ground.delta")[0]
        if (self.delta_l < 901) :
            self.state = State.Reading
                    
    def fill_lign(self) :
        self.delta = aseba.get("thymio-II", "prox.ground.delta")[1]
        self.statistics()
        self.update_ndev()             
        if abs(self.ndev) < self.SIGMA : # nous sommes sur la piste
            self.preg = int (0.6 * self.ndev)
            self.ireg = int (self.ireg + 0.33 * self.preg ) 
            self.left = self.speed + (self.preg+self.ireg)
            self.right = self.speed - (self.preg+self.ireg)
            
        else : # nous avons perdu la piste
            self.ireg = 0
            self.left = self.ndev/2
            self.right = -self.ndev/2
        aseba.set("thymio-II","motor.left.target",[self.left])
        aseba.set("thymio-II","motor.right.target",[self.right])
        
    def smove(self, spL, spR, dur, stDur):
        if self.moveS:
            if(self.timerOn == False) :
                self.init_timer(stDur)
                self.left = 100
                self.right = 100
                aseba.set("thymio-II","motor.left.target",[self.left])
                aseba.set("thymio-II","motor.right.target",[self.right])
            elif (self.times_up()) :
                self.reset_timer()
                self.moveS = False
            
            self.dec_timer()

        else :
            if(self.timerOn == False) :
                self.init_timer(dur)
                self.left = spL
                self.right = spR
                aseba.set("thymio-II","motor.left.target",[self.left])
                aseba.set("thymio-II","motor.right.target",[self.right])
            elif (self.times_up()) :
                self.reset_timer()
                self.state = State.Normal

            self.dec_timer()
    
    def move(self) :                
        if (robot.state == State.Normal) :         
            robot.left_sensor()
            self.fill_lign()
        elif(robot.state == State.Evaluate) :
            self.evaluate()

        elif (robot.state.name[0] == "R") :
            self.read_code(self.state)

        elif(robot.state == State.Stop ) :
            self.stop(5)
            self.reset_code()

        else :
            data = moveTypes.get(tuple(self.lastCode))
            self.smove(data[0],data[1],data[2],data[3])

        robot.main_timer+=1

  
def run(aseba,robot):
    robot.move()
        
    return True
 
if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-s", "--system", action="store_true", dest="system", default=False,help="use the system bus instead of the session bus")
 
    (options, args) = parser.parse_args()
    
    aseba = Aseba(options.system)

    robot = Robot(100)
    

    gobject.timeout_add (100, run, aseba,robot)
    aseba.run()

