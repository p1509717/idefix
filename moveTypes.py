import enum


class Color(enum.Enum):
   White = 1
   LightGrey = 2
   DarkGrey = 3
   Black = 4

smallLeft = [80, 130, 80, 20]
smallRight = [130, 80, 80, 20]

mediumLeft = [80, 114, 125, 30]
mediumRight = [114, 80, 125, 30]

largeLeft = [80, 103, 180, 37]
largeRight = [103, 80, 180, 37]

straight = [100, 100, 150, 1]

stop = [0, 0, -1, 1]

moveTypes = {
    #   Line moves
    (Color.DarkGrey, Color.DarkGrey, Color.White, Color.White)      : largeLeft,  #gauche large
    (Color.DarkGrey, Color.LightGrey, Color.White, Color.White)     : largeRight,  #droite large
    (Color.DarkGrey, Color.DarkGrey, Color.DarkGrey, Color.White)   : mediumLeft,  #gauche moyenne
    (Color.DarkGrey, Color.LightGrey, Color.DarkGrey, Color.White)  : mediumRight,  #droite moyenne
    (Color.DarkGrey, Color.DarkGrey, Color.LightGrey, Color.White)  : smallLeft,  #gauche courte
    (Color.DarkGrey, Color.LightGrey, Color.LightGrey, Color.White) : smallRight,  #droite courte

    #   Tri moves
    (Color.LightGrey, Color.White, Color.Black, Color.LightGrey)    : straight, #tout droite
    (Color.LightGrey, Color.White, Color.Black, Color.White)        : straight, #tout droite
    (Color.LightGrey, Color.DarkGrey, Color.White, Color.DarkGrey)  : mediumLeft, #gauche moyenne
    (Color.LightGrey, Color.LightGrey, Color.White, Color.DarkGrey) : smallRight, #droite courte
    (Color.LightGrey, Color.White, Color.DarkGrey, Color.Black)     : mediumLeft, #Stop + gauche moyenne
    (Color.LightGrey, Color.White, Color.LightGrey, Color.Black)    : smallRight, #Stop + droite courte
    (Color.LightGrey, Color.DarkGrey, Color.White, Color.White)     : mediumLeft, #gauche moyenne
    (Color.LightGrey, Color.LightGrey, Color.White, Color.LightGrey): smallRight, #droite courte
    (Color.LightGrey, Color.Black, Color.White, Color.Black)        : stop, #Stop

    #   Quad moves
    #(Color.Black, Color.LightGrey, Color.White, Color.White)        : [80, 103, 180,37], #
    #(Color.Black, Color.DarkGrey, Color.White, Color.White)        : [80, 103, 180,37], #
    #(Color.Black, Color.White, Color.DarkGrey, Color.White)        : [80, 103, 180,37], #
    #(Color.Black, Color.White, Color.LightGrey, Color.White)        : [80, 103, 180,37], #
    (Color.Black, Color.Black, Color.White, Color.White)        : stop, #
    (Color.Black, Color.White, Color.Black, Color.White)        : straight, #
    (Color.Black, Color.White, Color.White, Color.White)        : stop, #

    #   Straight move
    #(Color.White, Color.White, Color.White, Color.White)        : straight #
}